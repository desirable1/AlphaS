import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './Home';
import About from './About';
import Movie from './Movie';
import Contact from './Contact';
import Gallery from './Gallery';


class Root extends React.Component {
    render () {
        return(
            <div>
                <Switch>
                    <Route exact path = '/' component={Home} />
                    <Route path = '/about' component={About} />
                    <Route path = '/movie' component={Movie} />
                    <Route path = '/contact' component={Contact} />
                    <Route path = '/gallery' component={Gallery} />
                </Switch>
            </div>
        );
    }
    
}

export default Root;