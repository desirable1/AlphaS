import React from 'react';


const Contact = () => {
    return(
        <div className="contact">
        <main>
        <div className="container-fluid">
          <h1>Careers at AlphaStores</h1><br></br>
           <p>We are always looking for talented people to join our team. If you feel you have a talent that would enrich our studio, Please submit your information below and we’ll contact you if we have an availability.<br></br> </p>
        </div>
      <section className="container">
        <h2 className="section-heading h1 pt-4"><i className="fa fa-envelope"></i> Contact Us</h2><br></br>
        <div className="row">
            <div className="col-lg-5 mb-4">
                <div className="form-header">
                        <p>Best Movies for Your Pleasure..</p>
                      </div><br></br>
                        <div className="md-form">
                            <i className="fa fa-user prefix grey-text"></i>
                            <input type="text" id="form-name" className="form-control"/>
                            <label for="form-name">Your name</label>
                        </div>
                        <div className="md-form">
                            <i className="fa fa-envelope prefix grey-text"></i>
                            <input type="text" id="form-email" className="form-control"/>
                            <label for="form-email">Your email</label>
                        </div>
                        <div className="md-form">
                            <i className="fa fa-tag prefix grey-text"></i>
                            <input type="text" id="form-Subject" className="form-control"/>
                            <label for="form-Subject">Subject</label>
                        </div>
                        <div className="text-center mt-4">
                            <button className="btn btn-light-blue">Submit</button>
                        </div><br></br>
        
                <div className="row text-center">
                    <div className="col-md-4">
                    <p><a className="btn-floating blue accent-1"><i className="fa fa-map-marker"></i></a> San Francisco, CA 94126<br></br> United States</p>
                    </div>
                    <div className="col-md-4">
                        <p><a className="btn-floating blue accent-1"><i className="fa fa-phone"></i></a> + 01 234 567 89 <br></br>Mon - Fri, 8:00-22:00</p>
                    </div>
                    <div className="col-md-4">
                        <p><a className="btn-floating blue accent-1"><i className="fa fa-envelope"></i></a>alphastores@gmail.com </p>
                    </div>
                </div>
            </div> 
        </div>
    </section>
    </main>
    
        </div>
    );
}

export default Contact;