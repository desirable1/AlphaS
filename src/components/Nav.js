import React from 'react';
import { Link } from 'react-router-dom';

import img_logo from './images/head/logo.png';


const Nav = () => {
    return(
        <div>
        <nav className="navbar navbar-expand-lg navbar-dark black">
        <a className="navbar-brand" href="#">AlphaStores<img src={img_logo} alt="logo" className="logo"/></a>
         <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
           <span className="navbar-toggler-icon"></span>
              </button>
            <div className="collapse navbar-collapse" id="basicExampleNav">
             <ul className="navbar-nav mr-auto">
                  <li className="nav-item active">
                 <Link className="nav-link" to="/">Home</Link>
                     </li>
                    <li className="nav-item">
                     <Link className="nav-link" to="/about">About Us</Link>
                  </li>
                    <li className="nav-item">
                    <Link className="nav-link" to="/contact">Contact Us</Link>
                  </li>
                   <li className="nav-item dropdown">
               <a className="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Media</a>
               <div className="dropdown-menu" aria-labelledby="dropdown05">
                 <Link className="dropdown-item" to="/gallery">Gallery</Link>
                 <Link className="dropdown-item" to="/movie">Movies</Link>
               </div>
             </li>
                </ul>
   <form className="form-inline">
       <div className="md-form mt-0">
           <input className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"/>>
       </div>
   </form>
</div>
</nav>

        </div>
    );
}

export default Nav;