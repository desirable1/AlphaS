import React from 'react';
import './css/main.css';


import img_jeff from './images/head/jeff.jpg';
import img_pam from './images/head/pam.jpg';
import img_kunle from './images/head/kunle.jpg';
import img_oboli from './images/head/oboli.jpg';
import img_interns from './images/head/interns.jpg';

const About = () => {
    return(
        <div className="about">
        <div className="jumbotron about-jumbo"></div>
        <div className="container">
          <h1 className="display-4">Our story</h1>
          <p className="lead"> AlphaStores is an independent, multidisciplinary Stores Where Viewers come to Rent movies... We firmly believe in the intrinsic value entertainment brings and practice this belief by partnering with clients large and small across a wide range of industries and sectors...</p>
        </div>
      <div className="container-fluid">
          <h1>Executives</h1>
        <div className="row">
          <div className="col">
            <img src={img_jeff} alt="jeff"/>
            <p><b>Jeffrey</b></p>
            <p><em> Managing Director</em></p>
      </div>
            <div className="col">
            <img src={img_pam} alt="pam"/>
            <p><b>Pamela</b><br></br><em>CEO, AlphaStores</em></p>
          </div>
      </div>
      </div>
      
      <div className="container-fluid">
          <h1> Partners</h1><br></br>
        <div className="row">
          <div className="col">
            <img src={img_kunle}/>
            <p><b>Kunle Afolayan</b><br></br><em>Producer</em></p>
      </div>
            <div className="col">
            <img src={img_oboli}/>
            <p><b>Omoni Oboli</b><br></br><em>Best Seller</em></p>
          </div>
      </div>
      </div>
       <div className="row">
          <div className="col">
            <img src={img_pam}/>
            <p><b>Aishrya</b><br></br><em>Actress</em></p>
      
      </div>
            <div className="col">
            <img src={img_interns}/>
            <p><b>Students</b><br></br><em>Interns</em></p>
          </div>
      </div>
      </div>
    );
}

export default About;