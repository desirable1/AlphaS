import React from 'react';

import img_slide1 from './images/film/mov0.jpg';
import img_slide2 from './images/film/mov1.jpg';
import img_slide3 from './images/film/gal3.jpg';

const Movie = () => {
    return(
        <div>
        <div id="gallery" className="carousel slide carousel-fade" data-ride="carousel">
        <ol className="carousel-indicators">
          <li data-target="#gallery" data-slide-to="0" className="active"></li>
          <li data-target="#gallery" data-slide-to="1"></li>
           <li data-target="#gallery" data-slide-to="2"></li>
      </ol>
  <div className="carousel-inner" role="listbox">
      <div className="carousel-item active">
          <img className="d-block w-100 h-50" src={img_slide1} alt="gallery1"/>
      </div>
      <div className="carousel-item">
          <img className="d-block w-100 h-50" src={img_slide2} alt="Second slide"/>
      </div>
       <div className="carousel-item">
          <img className="d-block w-100 h-50" src={img_slide3} alt="Second slide"/>
      </div>
      </div>
      <a className="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="sr-only">Previous</span>
      </a>
      <a className="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="sr-only">Next</span>
      </a>
  </div><br></br>
    <div className="midn"></div><br></br>
   <main>
       <div className="container-fluid">
         <h2 id= "movies"></h2>
          <div className="row">
              <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                      <iframe width="100%" height="200" src="https://www.youtube.com/embed/WSWtCLBrrAk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                      </div>
                    </div>
              <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                      <iframe width="100%" height="200" src="https://www.youtube.com/embed/wswvNZveNHc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                      </div>
                         </div>
              <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/QYmREjN0s1U" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                      </div>
                      </div>
                     </div>
            <div className="row">
              <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                      <iframe width="100%" height="200" src="https://www.youtube.com/embed/9QAz-J68rYU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                      </div>
                    </div>
              <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                      <iframe width="100%" height="200" src="https://www.youtube.com/embed/sU3TRJiRobs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                      </div>
                         </div>
              <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                   <iframe width="100%" height="200" src="https://www.youtube.com/embed/OynWRG-kWGE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                      </div>
                      </div>
                     </div>
            <div className="row">
              <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                      <iframe width="100%" height="200" src="https://www.youtube.com/embed/qvRew9yfiPk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>   
                       </div>
                          </div>
               <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                      <iframe width="100%" height="200" src="https://www.youtube.com/embed/xoEeJrPT8-U" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/iVeN0CjX5BQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                  </div>
                </div>
              </div>
             <div className="row">
              <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                     <iframe width="100%" height="200" src="https://www.youtube.com/embed/iVeN0CjX5BQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>   
                       </div>
                          </div>
               <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                   <iframe width="100%" height="200" src="https://www.youtube.com/embed/iVeN0CjX5BQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="card mb-4 box-shadow">
                      <iframe width="100%" height="200" src="https://www.youtube.com/embed/6vUKAui3tbs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                  </div>
                </div>
            </div>
          </div>
        </main><br></br>

        </div>
    );
}

export default Movie;