import React, { Component } from 'react';
import './App.css';
import Root from './components/Root';
import Nav from './components/Nav';
import Footer from './components/Footer';


class App extends Component {
  render() {
    return (
      <div>
        <Nav />
        <Root/>
        <Footer />
      </div>
    );
  }
}

export default App;
